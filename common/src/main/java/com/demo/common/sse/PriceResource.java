package com.demo.common.sse;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("/sse3")
public class PriceResource {

    @Inject @Channel("prices")
    Emitter<String> emitter;

    @GET
    public void postAPrice() {
        emitter.send("hello");
        emitter.complete();
    }

}
