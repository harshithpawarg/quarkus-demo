package com.demo.common.sse;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.jboss.resteasy.annotations.SseElementType;
import org.reactivestreams.Publisher;

import io.smallrye.mutiny.Multi;

@Path("/sse2")
public class SseResource {

    // Get the prices stream
    @Inject
    @Channel("prices") Publisher<String> prices;

    @Transactional
    @GET
    @Path("/prices")
    @Produces(MediaType.SERVER_SENT_EVENTS)
    @SseElementType(MediaType.TEXT_PLAIN)
    public Publisher<String> prices() {
        // get the next three prices from the price stream
        return Multi.createFrom().publisher(prices).map(price -> {
            System.out.println("ttttttttttttt" + price);
            return price;
        });
    }
}
