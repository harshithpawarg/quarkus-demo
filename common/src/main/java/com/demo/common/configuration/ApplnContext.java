package com.demo.common.configuration;

public class ApplnContext {

    private static ThreadLocal<Boolean> isMigrationRunning =
        new ThreadLocal<Boolean>() {
            @Override public Boolean initialValue() {
                return false;
            }
        };

    public static Boolean getIsMigrationRunning() {
        return isMigrationRunning.get();
    }

    public static void setIsMigrationRunning(Boolean mode) {
        isMigrationRunning.set(mode);
    }
}
