package com.demo.common.configuration;

import com.demo.common.service.AutoWireHelper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AutoWireConfig {

    @Bean
    public AutoWireHelper autoWireHelper(){
        return AutoWireHelper.getInstance();
    }

}
