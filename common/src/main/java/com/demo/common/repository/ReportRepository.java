package com.demo.common.repository;

import com.demo.common.model.Address;
import com.demo.common.model.Report;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportRepository extends JpaRepository<Report, Long> {

}
