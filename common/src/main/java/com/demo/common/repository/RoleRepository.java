package com.demo.common.repository;

import com.demo.common.model.Role;
import com.demo.common.model.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    List<Role> findByNameAndTenant(String name, Tenant tenant);

    List<Role> findByTenant(Tenant tenant);
}
