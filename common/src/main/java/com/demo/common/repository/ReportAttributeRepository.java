package com.demo.common.repository;

import com.demo.common.model.ReportAttribute;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportAttributeRepository extends JpaRepository<ReportAttribute, Long> {

}
