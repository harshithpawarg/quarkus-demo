package com.demo.common.repository;

import com.demo.common.model.Party;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface PartyRepository extends JpaRepository<Party, Long> {


//    @Query(nativeQuery = true, value = " \n" +
//            "select p.id as id, p.name as name, p.email as email, p.phone_number as phoneNumber,  \n" +
//            "                    p.is_broker as isBroker, p.is_collector as isCollector,   \n" +
//            "                    p.is_funder as isFunder, p.is_representative as isRepresentative,   \n" +
//            "                    p.is_house_broker as isHouseBroker, p.is_external_funder as isExternalFunder,   \n" +
//            "                    p.is_investor as isInvestor, p.is_house_company as isHouseCompany,  \n" +
//            "                    p.is_merchant as isMerchant, p.account_number as accountNumber,  \n" +
//            "                    p.aba_number as abaNumber, p.funder_prefix as funderPrefix, p.bank_name as bankName,  \n" +
//            "                    p.contact_person as contactPerson, p.enable as enabled, \n" +
//            "a.address as street, a.city, a.zip_code as zipcode  \n" +
//            "                    from parties p left join addresses a on p.physical_address_id=a.id\n" +
//            "                    order by p.created_at desc;")
//    List<Map<String, Object>> findAllParty();


//    @Query(nativeQuery = true, value = " \n" +
//            "select p.id as id, p.name as name, p.email as email, p.phone_number as phoneNumber, p.tenant_id as tenantId, \n" +
//            "        p.is_broker as isBroker, p.is_collector as isCollector,  \n" +
//            "        p.is_funder as isFunder, p.is_representative as isRepresentative,  \n" +
//            "        p.is_house_broker as isHouseBroker, p.is_external_funder as isExternalFunder,  \n" +
//            "        p.is_investor as isInvestor, p.is_house_company as isHouseCompany, \n" +
//            "        p.is_merchant as isMerchant, p.account_number as accountNumber, \n" +
//            "        p.aba_number as abaNumber, p.funder_prefix as funderPrefix, p.bank_name as bankName, \n" +
//            "        p.contact_person as contactPerson,  \n" +
//            "        ad.address as address, ad.city as city, \n" +
//            "        ad.state as state, ad.zip_code as zipCode, p.enable as enabled " +
//            "        from parties p \n" +
//            "        left outer join addresses ad on (ad.id = p.physical_address_id) \n" +
//            "        where p.id = ?1")
//    List<Map<String, Object>> findByPartyId(Long partyId);
}
