package com.demo.common.repository;

import com.demo.common.model.User;
import com.demo.common.model.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserProfileRepository  extends JpaRepository<UserProfile, Long> {
    List<UserProfile> findByEntityName(String name);

    UserProfile findTop1ByEntityNameAndUser(String entityName, User user);
}
