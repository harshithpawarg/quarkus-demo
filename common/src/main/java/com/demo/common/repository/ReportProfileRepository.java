package com.demo.common.repository;

import com.demo.common.model.ReportProfile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportProfileRepository extends JpaRepository<ReportProfile, Long> {
}
