//package com.demo.common.repository;
//
//import org.springframework.data.jpa.repository.support.JpaEntityInformation;
//import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.persistence.EntityManager;
//import java.io.Serializable;
//
//
//public class CustomRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID> implements CustomRepository<T, ID> {
//
//    private final EntityManager entityManager;
//
//    public CustomRepositoryImpl(JpaEntityInformation entityInformation, EntityManager entityManager) {
//
//        super(entityInformation, entityManager);
//        this.entityManager = entityManager;
//    }
//
//    @Override
//    @Transactional
//    public void refresh(T t) {
//        System.out.println("Refresh: " + t.getClass().getName());
//        final T entity = entityManager.merge(t);
//        entityManager.refresh(entity);
//    }
//}
