package com.demo.common.repository;

import com.demo.common.model.Role;
import com.demo.common.model.Tenant;
import com.demo.common.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findByEmail(String email);

    User findByEmailIgnoreCase(String email);

    List<User> findByFullNameAndTenant(String fullName, Tenant tenant);

    List<User> findByTenant(Tenant tenant);

    List<User> findByTenantIdOrderByIdDesc(@Param("tenantId") Long tenantId);

    List<User> findByRolesAndTenant(Role role, Tenant tenant);

    List<User> findByIdIn(List<Long> userIds);

    List<User> findByFullName(String fullName);
}
