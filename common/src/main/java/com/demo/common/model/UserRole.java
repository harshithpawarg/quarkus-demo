package com.demo.common.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "user_roles")
@Audited(withModifiedFlag = true)
public class UserRole extends AuditModel {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "user_role_Sequence")
    @SequenceGenerator(name = "user_role_Sequence", sequenceName = "USER_ROLE_SEQUENCE", allocationSize = 1)

    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "role_id")
    private Long roleId;

    public UserRole() {}

    public UserRole(Long userId, Long roleId) {
        this.userId = userId;
        this.roleId = roleId;
    }

}
