package com.demo.common.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Map;

@Entity
@Getter
@Setter
@Table(name = "report_profiles")
public class ReportProfile extends AuditModel {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "report_profile_Sequence")
    @SequenceGenerator(name = "report_profile_Sequence", sequenceName = "REPORT_PROFILE_SEQUENCE", allocationSize = 1)
    private Long id;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Map<String, Object> info;

    @Column( name = "entity_name" )
    private String entityName;

    @Column( name = "report_name" )
    private String reportName;
}
