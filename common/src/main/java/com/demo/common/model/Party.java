package com.demo.common.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "parties")
@Audited(withModifiedFlag = true)
public class Party extends AuditModel {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "party_Sequence")
    @SequenceGenerator(name = "party_Sequence", sequenceName = "PARTY_SEQUENCE", allocationSize = 1)
    private Long id;

    @org.hibernate.annotations.Index(name = "parties_name_index")
    private String name;

    private String email;

    @Column( name = "phone_number" )
    @Audited(modifiedColumnName = "phone_number_mod")
    private String phoneNumber;

    @Column( name = "contact_person" )
    @Audited(modifiedColumnName = "contact_person_mod")
    private String contactPerson;

    @Column(name = "is_broker", columnDefinition = "boolean default false")
    @Audited(modifiedColumnName = "is_broker_mod")
    private Boolean isBroker = false;

    @Column(name = "is_investor", columnDefinition = "boolean default false")
    @Audited(modifiedColumnName = "is_investor_mod")
    private Boolean isInvestor = false;

    @Column(name = "is_merchant", columnDefinition = "boolean default false")
    @Audited(modifiedColumnName = "is_merchant_mod")
    private Boolean isMerchant = false;

    @Column(name = "is_funder", columnDefinition = "boolean default false")
    @Audited(modifiedColumnName = "is_funder_mod")
    private Boolean isFunder = false;

    @Column(name = "is_collector", columnDefinition = "boolean default false")
    @Audited(modifiedColumnName = "is_collector_mod")
    private Boolean isCollector = false;

    @Column(name = "is_house_company", columnDefinition = "boolean default false")
    @Audited(modifiedColumnName = "is_house_company_mod")
    private Boolean isHouseCompany = false;

    @Column(name = "is_external_funder", columnDefinition = "boolean default false")
    @Audited(modifiedColumnName = "is_external_funder_mod")
    private Boolean isExternalFunder = false;

    @Column(name = "is_representative", columnDefinition = "boolean default false")
    @Audited(modifiedColumnName = "is_representative_mod")
    private Boolean isRepresentative = false;

    @Column(name = "is_house_broker", columnDefinition = "boolean default false")
    @Audited(modifiedColumnName = "is_house_broker_mod")
    private Boolean isHouseBroker = false;

    @Column(name = "is_iso", columnDefinition = "boolean default false")
    @Audited(modifiedColumnName = "is_iso_mod")
    private Boolean isIso = false;

    @Column( name = "aba_number" )
    @Audited(modifiedColumnName = "aba_number_mod")
    private String abaNumber;

    @Column( name = "account_number" )
    @Audited(modifiedColumnName = "account_number_mod")
    private String accountNumber;

    @Column( name = "bank_name" )
    @Audited(modifiedColumnName = "bank_name_mod")
    private String bankName;

    @ManyToOne
    private Tenant tenant;

    @Column( name = "business_legal_name" )
    @Audited(modifiedColumnName = "business_legal_name_mod")
    private String businessLegalName;

    @Column( name = "federal_tax_id" )
    @Audited(modifiedColumnName = "federal_tax_id_mod")
    private String federalTaxId;

    @Column( name = "legal_entity" )
    @Audited(modifiedColumnName = "legal_entity_mod")
    private String legalEntity;

    @Column(name = "business_fax_id_1")
    @Audited(modifiedColumnName = "business_fax_id_1_mod")
    private String businessFaxId1;

    @Column(name = "business_fax_id_2")
    @Audited(modifiedColumnName = "business_fax_id_2_mod")
    private String businessFaxId2;

    @ManyToOne
    @JoinColumn(name = "physical_address_id")
    @Audited(modifiedColumnName = "physical_address_mod")
    private Address physicalAddress;

    @ManyToOne
    @JoinColumn(name = "mailing_address_id")
    @Audited(modifiedColumnName = "mailing_address_mod")
    private Address mailingAddress;

    @Temporal(value = TemporalType.DATE)
    @Column( name = "business_start_date" )
    @Audited(modifiedColumnName = "business_start_date_mod")
    private Date businessStartDate;

    @Column(name = "state_of_incorp")
    @Audited(modifiedColumnName = "state_of_incorp_mod")
    private String stateOfIncorp;

    @Column( name = "company_website" )
    @Audited(modifiedColumnName = "company_website_mod")
    private String companyWebsite;

    private Boolean favourite = false;

    @Column(name = "business_description")
    @Audited(modifiedColumnName = "business_description_mod")
    private String description;

    @Column(name = "business_property_type")
    @Audited(modifiedColumnName = "business_property_type_mod")
    private String propertyType;

    @Column(name = "business_payment", precision = 12, scale = 2)
    @Audited(modifiedColumnName = "business_payment_mod")
    private BigDecimal payments;

    @Column(name = "is_bankruptcy_open")
    @Audited(modifiedColumnName = "is_bankruptcy_open_mod")
    private Boolean  openBankruptcy;

    @Column( name = "landlord_name")
    @Audited(modifiedColumnName = "landlord_name_mod")
    private String  landlordName;

    @Column( name = "landlord_contact")
    @Audited(modifiedColumnName = "landlord_contact_mod")
    private String  landlordContact;

    @Column( name = "abbreviated_name")
    @Audited(modifiedColumnName = "abbreviated_name_mod")
    private String abbreviatedName;

    @Column( name = "merchant_no")
    @Audited(modifiedColumnName = "merchant_no_mod")
    private Long merchantNo;

    @Column( name = "funder_prefix")
    @Audited(modifiedColumnName = "funder_prefix_mod")
    private String funderPrefix;

    @Column(name = "enable", columnDefinition = "boolean default true")
    private Boolean enable = true;

    public Party() {

    }

}
