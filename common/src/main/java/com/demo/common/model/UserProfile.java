package com.demo.common.model;

import com.demo.common.model.AuditModel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Map;

@Entity
@Getter
@Setter
@Table(name = "user_report_profiles")
//@Audited(withModifiedFlag = false)
public class UserProfile extends AuditModel {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "user_report_profile_Sequence")
    @SequenceGenerator(name = "user_report_profile_Sequence", sequenceName = "USER_REPORT_PROFILE_SEQUENCE", allocationSize = 1)
    private Long id;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Map<String, Object> info;

    @Column(name = "entity_name")
    private String entityName;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
