package com.demo.common.controller;

import com.demo.common.model.Tenant;
import com.demo.common.model.User;
import com.demo.common.model.UserRole;
import com.demo.common.repository.TenantRepository;
import com.demo.common.repository.UserRepository;
import com.demo.common.repository.UserRoleRepository;
import com.demo.common.service.KeycloakService;
import com.demo.common.service.UserManager;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private TenantRepository tenantRepository;

    @Autowired
    private UserManager userManager;

    @Autowired
    private KeycloakService keycloakService;

    @Value("${spring.server.hostname}")
    private String HostURL;

    @PostMapping(path ="/getUserInfo")
    public Map<String, Object> getUserInformation(@RequestBody Map<String, Object> body) {

        System.out.println("REQUEST: /getUserInfo");
//        System.out.println(body.toString());

        String email = keycloakService.getCurrentUserEmail();
        System.out.println("tttttttttttt" + email);

        Map<String, Object> response = userManager.getUserInfo(email);

        System.out.println("RESPONSE: " + response.toString());

        return response;
    }

    @PostMapping(path ="/changeTenant")
    public boolean changeTenant(@RequestBody Map<String, Object> body) throws Exception {

//        System.out.println("REQUEST: /changeTenant");
//        System.out.println(body.toString());
//        String tenant = body.get("tenant").toString();
//
//        AccessToken accessToken = keycloakService.getAccessToken();
//
//        String email = accessToken.getEmail();
//        System.out.println(email);
//        boolean updated = keycloakService.updateCurrentTenant(email, tenant);
//
//        System.out.println("RESPONSE: " + updated);

        return true;
    }

    @PostMapping(path = "/createUser", consumes = "application/json", produces = "application/json")
    public Map<String, Object> createUser(@RequestBody Map<String, Object> body) throws Exception{

        String firstName = body.get("firstName") != null ? (String) body.get("firstName") : null;
        String middleName = body.get("middleName") != null ? (String) body.get("middleName") : null;
        String lastName = body.get("lastName") != null ? (String) body.get("lastName") : null;
        String email = body.get("email") != null ? (String) body.get("email") : null;
        //String userName = randomUserName(email);
        String password = randomPassword();
        Long tenantId = (Long.parseLong((String) body.get("tenantId")));
        Long id = body.get("id") != null ? Long.parseLong(body.get("id").toString()) : null;

        Map<String, Object> result = new HashMap<>();

        User user = null;

        if (id != null)
            user = userRepository.findById(id).get();
        else
            user = new User();

        if (firstName != null)
            user.setFirstName(firstName);

        if (middleName != null)
            user.setMiddleName(middleName);

        if (lastName != null)
            user.setLastName(lastName);

        if (email != null)
            user.setEmail(email);

        user.setUserName(email);
        user.setFullName(firstName +" " + middleName + " " +lastName);
        user.setPassword("test1234");

        if (user.getId() != null) {

            //user.setPassword(password);
            userRepository.save(user);
//            keycloakService.updateUser(user, "test1234");
            result.put("id", user.getId());
            result.put("email", user.getEmail());

        } else {

            Map<String, Object> keycloakResult = keycloakService.addUser(user);
            String secureUserId = keycloakResult.get("secureUserId") != null ? keycloakResult.get("secureUserId").toString() : null;
            boolean isNewTenantAdded = (boolean)keycloakResult.get("isNewTenantAdded");

            System.out.println("SecureUserId: " + secureUserId + ", " + keycloakResult);

            Optional<Tenant> tenantOptional = tenantRepository.findById(tenantId);
            Tenant tenant = tenantOptional.get();
            user.setTenant(tenant);
            if (secureUserId == null ) {
                result.put("success", false);
            } else if(secureUserId.equalsIgnoreCase("user exists")) {
                result.put("error", "User already exists in secure server");
            } else {
                user.setSecureUserId(secureUserId);
                try {
                    userRepository.save(user);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if(!isNewTenantAdded) {
                    result.put("newUser", true);
                    String userEmail = user.getEmail();
                    String mailContent = "Dear " +user.getFullName()+", <br />"+
                        "  Please find your credentials below: <br />" +
                        "  URL: " + HostURL + "<br />"+
                        "  Username: " + user.getUserName() + "<br />"+
                        "  Password: " + user.getPassword();
                    JSONArray recipients = new JSONArray();
                    recipients.put(userEmail);
                    //emailService.sendMail("New user created for you", mailContent, recipients, null);
                }
                result.put("id", user.getId());
                result.put("email", user.getEmail());

            }
        }
        return result;
    }

    private String randomPassword() {
        String sampleString = "abcdefghijklmnopqrstuvwxyx";
        StringBuilder sb = new StringBuilder(10);
        for (int i = 0; i < 8; i++) {
            int index = (int)(sampleString.length() * Math.random());
            sb.append(sampleString.charAt(index));
        }
        String password = sb.toString();
        System.out.println("dynamically genarated password: " + password);
        return password;
    }


    @GetMapping("/fetchUsersByTenant/{tenantId}")
    public String fetchByTenantId(@PathVariable("tenantId") Long tenantId) {
        List<User> users = userRepository.findByTenant(tenantRepository.findById(tenantId).get());
        int count = 0;

        JSONArray userItems = new JSONArray();
        for (User user : users) {
            count = count + 1;
            JSONObject userItem = new JSONObject();
            userItem.put("id", user.getId());
            userItem.put("index", count);
            userItem.put("username", user.getUserName());
            userItem.put("email", user.getEmail());
            userItem.put("firstName", user.getFirstName());
            userItem.put("middleName", user.getMiddleName());
            userItem.put("lastName", user.getLastName());
            userItem.put("fullName", user.getFullName());
            userItem.put("password", user.getPassword());
            userItem.put("createdAt", user.getCreatedAt());
            userItem.put("updatedAt", user.getUpdatedAt());
            userItem.put("secureUserId", user.getSecureUserId());
            userItems.put(userItem);
        }

        return userItems.toString();
    }

    @PostMapping(path = "/getUser")
    public Map<String,Object> getUser(@RequestBody Map<String,Object> body) {

        Map<String,Object> userRecord = new HashMap<>();
        Map<String,Object> result = new HashMap<>();
        Long id = body.get("id") != null ? Long.parseLong(body.get("id").toString()) : null;

        if (id != null) {
            User user = userRepository.findById(id).get();
            if (user != null) {
                userRecord.put("id", user.getId());
                userRecord.put("username", user.getUserName());
                userRecord.put("email", user.getEmail());
                userRecord.put("firstName", user.getFirstName());
                userRecord.put("middleName", user.getMiddleName());
                userRecord.put("lastName", user.getLastName());
                userRecord.put("roleName", user.getRoles().size() > 0? user.getRoles().get(0).getName() : "");
            }
        }
        result.put("userDetails", userRecord);
        return result;
    }

    @GetMapping("/fetchUsers/{tenantId}")
    public String fetchUsers(@PathVariable("tenantId") Long tenantId) throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        List<User> userList = userRepository.findByTenant(tenantRepository.findAll().get(0));
        JSONArray users = new JSONArray();
        for (User user : userList) {
            String birthDate = null;
            if (user.getBirthDate() != null)
                birthDate = simpleDateFormat.format(user.getBirthDate());
            JSONObject userDetails = new JSONObject();
            userDetails.put("userId", user.getId());
            userDetails.put("firstName", user.getFirstName());
            userDetails.put("lastName", user.getLastName());
            userDetails.put("middleName", user.getMiddleName());
            userDetails.put("dateOfBirth", birthDate);
            userDetails.put("socialSecurityNo", user.getSocialSecurityNumber());
            userDetails.put("emailAddress", user.getEmail());
            userDetails.put("cellPhone", user.getPhoneNumber());
            userDetails.put("middleName", user.getMiddleName() != null ? user.getMiddleName() : "");
            userDetails.put("fullName", user.getFullName() != null ? user.getFullName() : "");
            JSONObject address = new JSONObject();
//            Address userAddress = user.getAddress();
//            if (userAddress != null) {
//                address.put("address", userAddress.getAddress());
//                address.put("city", userAddress.getCity());
//                address.put("state", userAddress.getState());
//                address.put("zipCode", userAddress.getZipCode());
//            }
//            MediaAttachment userLicense = user.getDrivingLicense();
//            Map<String, Object> license = new HashMap<>();
//            if (userLicense != null) {
//                String bucketName = awsService.getCurrentTenantBucket();
//                String s3Url = "https://s3.amazonaws.com/" + bucketName + "/" + userLicense.getS3Url();
//                license.put("fileName", userLicense.getName());
//                license.put("s3Url", s3Url);
//                license.put("mediaId", userLicense.getId());
//            }
//            userDetails.put("license", license);
//            userDetails.put("userAddress", address);
            users.put(userDetails);
        }
        return users.toString();
    }

    @PostMapping(path ="/deleteKeyCloakUser")
    public Map<String, Object> deleteKeyCloakUser(@RequestBody Map<String, Object> body) {
        System.out.println("REQUEST: deleteKeyCloakUser");
        System.out.println(body.toString());
        String secureUserId = body.get("secureUserId").toString();
        System.out.println("user to delete " + secureUserId);

//        Boolean res = keycloakService.deleteUser(secureUserId);
        System.out.println("@@@@@deleteKeyCloakUser result: " );

        Map<String, Object> result = new HashMap<>();
        result.put("result", true);
        return result;
    }

    @PostMapping(path ="/updateKeyCloakUser")
    public Map<String, Object> updateKeyCloakUser(@RequestBody Map<String, Object> body) {
        Long id = Long.parseLong(body.get("userId").toString());
        User user = userRepository.findById(id).get();
        String password = body.get("password") != null ? body.get("password").toString() : null;
        System.out.println("user to update " + user.getEmail());
        System.out.println("user to update " + user.getId());
        //String decryptedPassword = passwordDecryptionService.decryptPassword(password, user.getEmail());
//        Boolean res = keycloakService.updateUser(user, password);
        Map<String, Object> result = new HashMap<>();
        result.put("result", true);
        return result;
    }

    @GetMapping("/fetchAllUsers")
    public List<Map<String, Object>> fetchAllUsers() {
        System.out.println("com.demo.auditing.controller => userController, request => fetchAllUsers");
        List<User> users = userRepository.findAll();
        List<Map<String, Object>> response = new ArrayList<>();
        for (User user : users) {
            Map<String, Object> userItem = new HashMap<>();
            userItem.put("id", user.getId());
            userItem.put("username", user.getUserName());
            userItem.put("email", user.getEmail());
            userItem.put("firstName", user.getFirstName());
            userItem.put("middleName", user.getMiddleName());
            userItem.put("lastName", user.getLastName());
            userItem.put("fullName", user.getFullName());
            response.add(userItem);
        }
        return response;
    }

    @GetMapping("/deleteUser/{userId}")
    public Map<String, Object> deleteUser(@PathVariable("userId") Long userId) {
        Map<String, Object> result = new HashMap<>();
        UserRole userRole = userRoleRepository.findByUserId(userId);
        if (userRole != null)
            userRoleRepository.deleteByUserId(userId);

        userRepository.deleteById(userId);

        result.put("success", true);
        return result;
    }

    @GetMapping("/getConfiguration/{tenantId}")
    public String getConfiguration(@PathVariable("tenantId") Long tenantId) {
        JSONObject response = new JSONObject();
        Tenant tenant = tenantRepository.findById(tenantId).get();
        response.put("profilePath",tenant.getProfilePath());
        return response.toString();
    }

//    public File convert(MultipartFile file) throws  Exception {
//        String uuidToken = UUID.randomUUID().toString();
//        File convFile = new File("JasperReportGenerator/Documents/"+ uuidToken+ "_" +file.getOriginalFilename());
//        convFile.createNewFile();
//        FileOutputStream fos = new FileOutputStream(convFile);
//        fos.write(file.getBytes());
//        fos.close();
//        return convFile;
//    }


}
