package com.demo.common.controller;


import com.demo.common.model.*;
import com.demo.common.repository.PartyRepository;
import com.demo.common.repository.ReportRepository;
import com.demo.common.service.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
//import org.springframework.http.codec.ServerSentEvent;
//import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import javax.transaction.Transactional;
import javax.transaction.Transactional.*;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@Transactional(value = TxType.REQUIRES_NEW, rollbackOn = Exception.class)
public class PartyController {

    @Autowired
    private PartyManager partyManager;

    @Autowired
    private PartyRepository partyRepository;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private TenantManager tenantManager;

    @Autowired
    private AddressManager addressManager;

    //@Autowired
    //private NotificationEventPublisher notificationEventPublisher;

    @Autowired
    private ReportRepository reportRepository;

    @PostMapping(value = "fetchPartyById")
    public Map<String, Object> fetchParty(@RequestBody Map<String, Object> body) {
        Map<String, Object> result = new HashMap<>();
        Map<String, Object> party = partyManager.findByPartyId(Long.parseLong(body.get("id").toString()));
        Map<String, Object> record = new HashMap<>();
        record.put("id", party.get("id"));
        record.put("name", party.get("name"));
        record.put("externalPartyNumber", party.get("externalPartyNumber"));
        record.put("email", party.get("email"));
        record.put("phoneNumber", party.get("phoneNumber"));
        record.put("isBroker", party.get("isBroker"));
        record.put("isCollector", party.get("isCollector"));
        record.put("isFunder", party.get("isFunder"));
        record.put("isRepresentative", party.get("isRepresentative"));
        record.put("isHouseBroker", party.get("isHouseBroker"));
        record.put("isExternalFunder", party.get("isExternalFunder"));
        record.put("isInvestor", party.get("isInvestor"));
        record.put("isHouseCompany", party.get("isHouseCompany"));
        record.put("merchantData", party.get("isMerchant"));
        record.put("accountNumber", party.get("accountNumber"));
        record.put("abaNumber", party.get("abaNumber"));
//        record.put("managementFeePercentage", party.get("managementFeePercentage"));
        record.put("funderPrefix", party.get("funderPrefix"));
        record.put("contactPerson", party.get("contactPerson"));
        record.put("emailPreferences", party.get("emailPreferences"));
        record.put("enabled", party.get("enabled"));


        record.put("tenantId", party.get("tenantId"));
        record.put("bankName", party.get("bankName") != null ? party.get("bankName") : "");

        Map<String, Object> address = new HashMap<>();

        if (party.get("address") != null)
            address.put("address", party.get("address"));


        if (party.get("city") != null)
            address.put("city", party.get("city"));

        if (party.get("state") != null)
            address.put("state", party.get("state"));

        if (party.get("zipCode") != null)
            address.put("zipCode", party.get("zipCode"));

        record.put("address", address);
        result.put("partyDetails", record);
        return result;
    }

//    @GetMapping("/sse-server/stream-sse")
//    public Flux<ServerSentEvent<String>> streamEvents() {
//        System.out.println("Streaming event from server");
//        return Flux.interval(Duration.ofSeconds(5))
//                .map(sequence -> ServerSentEvent.<String> builder()
//                        .id(String.valueOf(sequence))
//                        .event("periodic-event")
//                        .data(partyManager.getPartyData().toString())
//                        .build());
//    }

    @GetMapping("/fetchAllParties")
    public List<Map<String, Object>> getAllParties() {

        System.out.println("Request: fetchAllParties ");

        List<Map<String, Object>> resultSet = partyManager.getAllParties();
        return resultSet;
    }

    @PostMapping(path = "/createOrUpdateParty")
    public Map<String,Object> createOrUpdate(@RequestBody Map<String,Object> body) {

        System.out.println("body = " + body);

        Map<String,Object> result = new HashMap<>();

        Party party = null;
        Long id = body.get("id") != null ? Long.parseLong(body.get("id").toString()) : null;

        if (id != null)
            party = partyManager.getParty(id);
        else
            party = new Party();

        party.setName(validationService.isPresent(body.get("name")) ?
                body.get("name").toString() : null);
        party.setContactPerson(validationService.isPresent(body.get("contactPerson")) ?
                body.get("contactPerson").toString() : null);
        party.setPhoneNumber(validationService.isPresent(body.get("phoneNumber")) ?
                body.get("phoneNumber").toString() : null);
        party.setEmail(validationService.isPresent(body.get("email")) ?
                body.get("email").toString() : null);
        party.setAbaNumber(validationService.isPresent(body.get("abaNumber")) ?
                body.get("abaNumber").toString() : null);
        party.setBankName(validationService.isPresent(body.get("bankName")) ?
                body.get("bankName").toString() : null);
        party.setAccountNumber(validationService.isPresent(body.get("accountNumber")) ?
                body.get("accountNumber").toString() : null);

        party.setIsBroker(validationService.isPresent(body.get("isBroker")) ?
                (Boolean) body.get("isBroker") : party.getIsBroker() != null ? party.getIsBroker() : false);
        party.setIsCollector(validationService.isPresent(body.get("isCollector")) ?
                (Boolean) body.get("isCollector") : party.getIsCollector() != null ? party.getIsCollector() : false);
        party.setIsFunder(validationService.isPresent(body.get("isFunder")) ?
                (Boolean) body.get("isFunder") : party.getIsFunder() != null ? party.getIsFunder() : false);
        party.setIsInvestor(validationService.isPresent(body.get("isInvestor")) ?
                (Boolean) body.get("isInvestor") : party.getIsInvestor() != null ? party.getIsInvestor() : false);
        party.setIsRepresentative(validationService.isPresent(body.get("isRepresentative")) ?
                (Boolean) body.get("isRepresentative") : party.getIsRepresentative() != null ? party.getIsRepresentative() : false);
        party.setIsExternalFunder(validationService.isPresent(body.get("isExternalFunder")) ?
                (Boolean) body.get("isExternalFunder") : party.getIsExternalFunder() != null ? party.getIsExternalFunder() : false);
        party.setIsHouseBroker(validationService.isPresent(body.get("isHouseBroker")) ?
                (Boolean) body.get("isHouseBroker") : party.getIsHouseBroker() != null ? party.getIsHouseBroker() : false);
        party.setIsHouseCompany(validationService.isPresent(body.get("isHouseCompany")) ?
                (Boolean) body.get("isHouseCompany") : party.getIsHouseCompany() != null ? party.getIsHouseCompany() : false);
        party.setIsMerchant(validationService.isPresent(body.get("merchantData")) ?
                (Boolean) body.get("merchantData") : party.getIsMerchant() != null ? party.getIsMerchant() : false);

        if (party.getIsFunder() && body.get("funderPrefix") != null) {
            party.setFunderPrefix(body.get("funderPrefix").toString().toUpperCase());
        } else {
            if (body.get("funderPrefix") != null)
                party.setFunderPrefix(null);
        }

        Address physicalAddress = party.getPhysicalAddress() != null ? party.getPhysicalAddress() : new Address();
        if (validationService.isPresent(body.get("address"))) {
            Map<String, Object> addressData = (Map<String, Object>) body.get("address");
            addressManager.createAddress(physicalAddress, addressData);
            party.setPhysicalAddress(physicalAddress);
        }

        partyManager.save(party);
//        notificationEventPublisher.publish("party.createdOrUpdated", party);
        result.put("result", true);
        partyManager.sendPartyDetails();

        return result;
    }

}

