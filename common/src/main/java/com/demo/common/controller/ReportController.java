package com.demo.common.controller;

import com.demo.common.model.Report;
import com.demo.common.model.ReportAttribute;
import com.demo.common.model.Role;
import com.demo.common.model.User;
import com.demo.common.repository.ReportAttributeRepository;
import com.demo.common.repository.ReportRepository;
import com.demo.common.repository.UserRepository;
import com.demo.common.service.KeycloakService;
import com.demo.common.service.MqttManager;
import com.demo.common.service.MultipartBody;
import com.demo.common.service.UserManager;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
//import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.MultivaluedMap;
import java.io.*;
import java.util.*;

@RestController
@RequestMapping("/api")
@Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = Exception.class)
public class ReportController {

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private ReportAttributeRepository reportAttributeRepository;

    @Autowired
    private KeycloakService keycloakService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserManager userManager;

    @Autowired
    private MqttManager mqttManager;

    @GetMapping("mqttTest")
    public void Test() {
        mqttManager.test();
    }

    @GetMapping("/getJasperReports")
    public List<Map<String, Object>> getJasperReports() {
        List<Map<String, Object>> result = new ArrayList<>();

        List<Report> reports = reportRepository.findAll(Sort.by(new Sort.Order(Sort.Direction.DESC, "createdAt")));
        for (Report report : reports) {
            JSONArray reportAttributeList = new JSONArray();
            Map<String, Object> map = new HashMap<>();
            map.put("fileName", report.getFileName());
            map.put("id", report.getId());
            map.put("reportName", report.getName());
            map.put("filePath", report.getFilePath());
            map.put("permission", report.getPermission());
            List<ReportAttribute> reportAttributes = report.getReportAttributes();
            for (ReportAttribute reportAttribute : reportAttributes) {
                reportAttributeList.put(reportAttribute.getName());
            }
            map.put("selectedAttributes", reportAttributeList.join(",").replace("\"", ""));
            result.add(map);
        }
        return result;
    }

    @GetMapping("/getJasperReportBasedOnRoles")
    public List<Map<String, Object>> getJasperReportBasedOnRoles() throws Exception{
        List<Map<String, Object>> result = new ArrayList<>();
        List<User> userList = userRepository.findByEmail(keycloakService.getCurrentUserEmail());
        User user = null;
        if (userList.size() > 0) {
            user = userList.get(0);
        }
        List<String> userPermissions = new ArrayList<>();
        List<Role> roles = user.getRoles();
        for (Role role : roles) {
            userPermissions.add(role.getName());
        }

        System.out.println("userPermission" + userPermissions);

        List<Report> reports = reportRepository.findAll(Sort.by(new Sort.Order(Sort.Direction.DESC, "createdAt")));
        for (Report report : reports) {
            System.out.println("reportPermission" + report.getPermission());
            if (report.getPermission().equals("both") || userPermissions.contains(report.getPermission())) {
                JSONArray reportAttributeList = new JSONArray();
                Map<String, Object> map = new HashMap<>();
                map.put("fileName", report.getFileName());
                map.put("id", report.getId());
                map.put("reportName", report.getName());
                map.put("filePath", report.getFilePath());
                map.put("permission", report.getPermission());
                List<ReportAttribute> reportAttributes = report.getReportAttributes();
                for (ReportAttribute reportAttribute : reportAttributes) {
                    reportAttributeList.put(reportAttribute.getName());
                }
                map.put("selectedAttributes", reportAttributeList.join(",").replace("\"", ""));
                result.add(map);
            }
        }
        return result;
    }

    private File writeFile(byte[] content, String filename) throws IOException {

        File file = new File(filename);

        if (!file.exists()) {
            file.createNewFile();
        }

        FileOutputStream fop = new FileOutputStream(file);

        fop.write(content);
        fop.flush();
        fop.close();
        return file;

    }

    private String getFileName(MultivaluedMap<String, String> header) {
        String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

        for (String filename : contentDisposition) {
            if ((filename.trim().startsWith("filename"))) {

                String[] name = filename.split("=");

                String finalFileName = name[1].trim().replaceAll("\"", "");
                return finalFileName;
            }
        }
        return "unknown";
    }

    @RequestMapping(value = "/createOrUpdateReport", method = RequestMethod.POST, consumes = "multipart/form-data")
    public Map<String, Object> createOrUpdateReport(MultipartFormDataInput input ) throws Exception {
        System.out.println("inputttttttt" + input);
        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
        System.out.println("uuuuuuuu" + uploadForm.get("file"));
        System.out.println("uuuuuuuu" + uploadForm.get("reportDetails"));
        JSONObject jsonObject = new JSONObject();
        String fileName = "";
        File file = new File("/tmp/");
        for (InputPart inputPart : uploadForm.get("reportDetails")) {
            jsonObject = new JSONObject(inputPart.getBody(String.class, null));
        }


        for (InputPart inputPart : uploadForm.get("file")) {

            try {

                MultivaluedMap<String, String> header = inputPart.getHeaders();
                fileName = getFileName(header);

                //convert the uploaded file to inputstream
                InputStream inputStream = inputPart.getBody(InputStream.class, null);

                byte[] bytes = IOUtils.toByteArray(inputStream);

                //constructs upload file path
                String filePath = "/tmp/" + fileName;

                file = writeFile(bytes, filePath);

                System.out.println("Done" + filePath);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Map<String, Object> result = new HashMap<>();
        Report report = null;
        if (jsonObject.has("id")) {
            report = reportRepository.findById(Long.parseLong(jsonObject.get("id").toString())).get();
            reportAttributeRepository.deleteInBatch(report.getReportAttributes());
        } else
            report = new Report();
        //if (multipartFile != null) {
            report.setFileName(fileName);
            //File file = convert(multipartFile);
            report.setFilePath(file.getPath());
       // }
        report.setName(jsonObject.get("reportName").toString());
        report.setPermission(jsonObject.get("permission").toString());
        reportRepository.save(report);

        String reportAttributes = jsonObject.getString("selectedAttributes");
        String[] arr = reportAttributes.split(",");
        for (String attr : arr) {
            ReportAttribute reportAttribute = new ReportAttribute();
            reportAttribute.setName(attr);
            reportAttribute.setReport(report);
            reportAttributeRepository.save(reportAttribute);
        }
//

        result.put("success", true);
        return result;
    }

    @PostMapping(path="/generateJasperLetter")
    public Map<String, Object> generateJasperLetter(@RequestBody Map<String, Object> body
    ) throws Exception {
        System.out.println("bbbbbbbbbbbbb"+ body);
        Map<String, Object> attr = (Map<String, Object>)body.get("generatedAttributes");
        Report report = reportRepository.findById(Long.parseLong(body.get("id").toString())).get();
        Map<String, Object> res = new HashMap<>();
        String path = userManager.generateLetter(report, attr);
        res.put("path", path);
        return res;
    }

    @GetMapping(path = "/downloadJasperLetter/{filePath}")
    public ResponseEntity<InputStreamResource> downloadJasperLetter(@PathVariable("filePath") String path) throws Exception {
        File file = new File("../resources/jasperReport/" + path);
        ByteArrayInputStream in = retrieveByteArrayInputStream(file);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=" + file.getName());

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
    }

    public File convert(MultipartFile file) throws  Exception {
        String uuidToken = UUID.randomUUID().toString();
        File convFile = new File("JasperReportGenerator/Documents/"+ uuidToken+ "_" +file.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    public ByteArrayInputStream retrieveByteArrayInputStream(File file) throws  Exception {
        return new ByteArrayInputStream(FileUtils.readFileToByteArray(file));
    }
}
