package com.demo.common.controller;

import com.demo.common.service.SsePushNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@RestController
//@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/sse")
public class ServerEventsController {

    @Autowired
    SsePushNotificationService service;

    @RequestMapping(value = "/notifications", headers = "Accept=*/*",
            produces = MediaType.TEXT_PLAIN_VALUE)
    public SseEmitter doNotify() throws Exception {
        System.out.println("Streaming event from server");
        System.out.println("Controller Thread Name " + Thread.currentThread().getName());
        final SseEmitter emitter = new SseEmitter(0L);
        service.addEmitter(emitter);
        //service.doNotify(null);
        emitter.onCompletion(() -> service.removeEmitter(emitter));
        emitter.onTimeout(() -> service.removeEmitter(emitter));
        return emitter;
//        return ResponseEntity
//                .ok()
//                .contentType(MediaType.TEXT_EVENT_STREAM)
//                .body(emitter);
    }
}
