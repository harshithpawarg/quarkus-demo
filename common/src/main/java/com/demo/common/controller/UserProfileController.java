package com.demo.common.controller;

import com.demo.common.model.ReportProfile;
import com.demo.common.model.User;
import com.demo.common.repository.ReportProfileRepository;
import com.demo.common.repository.UserProfileRepository;
import com.demo.common.service.UserManager;
import com.demo.common.service.UserProfileManager;
import com.demo.common.model.UserProfile;
import com.demo.common.service.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class UserProfileController {

    @Autowired
    private ValidationService validationService;

    @Autowired
    private UserProfileManager userProfileManager;

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Autowired
    private ReportProfileRepository reportProfileRepository;

    @Autowired
    private UserManager userManager;

    @PostMapping("/createOrUpdateUserProfile")
    public Map<String, Object> createOrUpdateUserProfile(@RequestBody Map<String, Object> body) {
        Map<String, Object> response = new HashMap<>();
        if(validationService.isPresent(body.get("name")) && validationService.isPresent(body.get("name"))){
            String name = body.get("name").toString();
            Map<String, Object> info = (Map<String, Object>) body.get("info");
            response = userProfileManager.createOrUpdateUserProfile(name, info);
        }
        return response;
    }

    @PostMapping("/getUserProfile")
    public Map<String, Object> getUserProfile(@RequestBody Map<String, Object> body) {
        Map<String, Object> response = new HashMap<>();
        if(validationService.isPresent(body.get("name"))){
            String name = body.get("name").toString();
            UserProfile userProfile = userProfileManager.getUserProfileByName(name);
            if(userProfile != null)
                response = userProfile.getInfo();
        }
        return response;
    }

    @GetMapping("/getUserReportProfiles/{entityName}/{userId}")
    public Map<String, Object> getUserReportProfiles(@PathVariable("entityName") String entityName,
                                                     @PathVariable("userId") Long userId) throws Exception {
        Map<String, Object> result = new HashMap<>();
        User user = userManager.getUser(userId);
        UserProfile userProfile = userProfileRepository.findTop1ByEntityNameAndUser(entityName, user);
        if (userProfile != null) {
            result.put("info", userProfile.getInfo());
        }
        System.out.println("Result" + result);
        return result;
    }

    @GetMapping("/getReportProfiles")
    public List<Map<String, Object>> getReportProfiles() throws Exception {
        List<Map<String, Object>> result = new ArrayList<>();
        List<ReportProfile> reportProfiles = reportProfileRepository.findAll(Sort.by(new Sort.Order(Sort.Direction.DESC, "createdAt")));
        for (ReportProfile reportProfile : reportProfiles) {
            Map<String, Object> repProfile = new HashMap<>();
            repProfile.put("reportName", reportProfile.getReportName());
            repProfile.put("info", reportProfile.getInfo());
            result.add(repProfile);
        }
        return result;
    }


    @PostMapping("/saveColumnChooserReport")
    public Map<String, Object> saveColumnChooserReport(@RequestBody Map<String, Object> body) {
        Map<String, Object> result = new HashMap<>();
        Map<String, Object> customizeData = (Map<String, Object>) body.get("customizeData");
        List<String> reOrderColumns = (List<String>) body.get("columnsOrder");
        String reportName = body.get("reportName").toString();
        String entityName = body.get("entityName").toString();
        Map<String, Object> map = new HashMap<>();
        map.put("columnsOrder", reOrderColumns);
        map.put("columnsInfo", customizeData);

        ReportProfile reportProfile = new ReportProfile();
        reportProfile.setEntityName(entityName);
        reportProfile.setReportName(reportName);
        reportProfile.setInfo(map);
        reportProfileRepository.save(reportProfile);
        return result;
    }

    @PostMapping(value = "/saveColumnChooserPreset", consumes = "application/json", produces = "application/json")
    public Map<String, Object> saveColumnChooserPreset(@RequestBody Map<String, Object> body) {
        Map<String, Object> result = new HashMap<>();
        Map<String, Object> customizeData = (Map<String, Object>) body.get("customizeData");
        List<String> reOrderColumns = (List<String>) body.get("columnsOrder");
        Long userId = Long.parseLong(body.get("userId").toString());
        String entityName = body.get("entityName").toString();
        User user = userManager.getUser(userId);
        UserProfile userProfile = userProfileRepository.findTop1ByEntityNameAndUser(entityName, user);
        if (userProfile == null) {
            userProfile = new UserProfile();
        }
        Map<String, Object> map = new HashMap<>();
        if (userProfile.getInfo() != null) {
           map = userProfile.getInfo();
        }

        map.put("columnsOrder", reOrderColumns);
        map.put("columnsInfo", customizeData);
        userProfile.setEntityName(entityName);
        userProfile.setUser(user);
        userProfile.setInfo(map);
        userProfileRepository.save(userProfile);
        return result;
    }
}
