package com.demo.common.service;

public class AuditContext {

//    private static ThreadLocal<Boolean> enableAuditMode = new ThreadLocal<>();

    private static ThreadLocal<Boolean> auditMode =
        new ThreadLocal<Boolean>() {
            @Override public Boolean initialValue() {
                return true;
            }
        };

    public static Boolean getAuditMode() {
        return auditMode.get();
    }

    public static void setAuditMode(Boolean mode) {
        auditMode.set(mode);
    }

}
