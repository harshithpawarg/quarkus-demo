package com.demo.common.service;

import com.demo.common.model.Role;
import com.demo.common.model.Tenant;
import com.demo.common.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RoleManager {

    @Autowired
    private RoleRepository roleRepository;

    public List<Role> findByNameAndTenant(String admin, Tenant tenant) {
        return roleRepository.findByNameAndTenant(admin, tenant);
    }

    public List<Role> findByTenant(Tenant tenant) {
        return  roleRepository.findByTenant(tenant);
    }
}
