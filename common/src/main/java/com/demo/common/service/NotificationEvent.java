package com.demo.common.service;

import org.springframework.context.ApplicationEvent;

public class NotificationEvent extends ApplicationEvent {
    private String topic;
    private Object data;
    private Object response;

    public NotificationEvent(Object source, String topic, Object data) {
        super(source);
        this.topic = topic;
        this.data = data;
    }
    public String getTopic() {
        return topic;
    }

    public Object getData() {
        return data;
    }

    public void setResponse(Object response) {
        this.response = response;
    }

    public Object getResponse() {
        return response;
    }
}
