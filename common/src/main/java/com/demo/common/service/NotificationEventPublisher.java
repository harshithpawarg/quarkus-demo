//package com.demo.common.service;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationEventPublisher;
//import org.springframework.stereotype.Component;
//
//@Component
//public class NotificationEventPublisher {
//
//    @Autowired
//    private ApplicationEventPublisher applicationEventPublisher;
//
//    public NotificationEvent publish(final String topic, Object data) {
//        System.out.println("Publishing Notification event ========= "+ topic);
//        NotificationEvent notificationEvent = new NotificationEvent(this, topic, data);
//        applicationEventPublisher.publishEvent(notificationEvent);
//        return notificationEvent;
//    }
//}
