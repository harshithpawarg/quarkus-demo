package com.demo.common.service;


import com.demo.common.model.Party;
import com.demo.common.model.User;
import com.demo.common.repository.PartyRepository;
import com.demo.common.repository.UserRepository;
import io.quarkus.scheduler.Scheduled;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

@Service
public class SsePushNotificationService {

    @Autowired
    PartyManager partyManager;

    final DateFormat DATE_FORMATTER = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
    final List<SseEmitter> emitters = new CopyOnWriteArrayList<>();

//    @Autowired
    private MqttManager mqttManager;

    private static final Map<SseEmitter, String> SUBSCRIBERS  = new HashMap<>();

    private static final Map<String, Pattern>    PATTERNS     = new HashMap<>();

    private void forwardToSSE(final String TOPIC, final JSONObject JSON) {
        SUBSCRIBERS.keySet().forEach(subscriber -> {
            final String  SUBSCRIBED_TOPIC   = SUBSCRIBERS.get(subscriber);
            final boolean MATCH_TOPIC        = SUBSCRIBED_TOPIC.equals(TOPIC); // exact same topic
            final boolean MATCH_SINGLE_LEVEL = PATTERNS.get(SUBSCRIBED_TOPIC).matcher(TOPIC).matches();
            final boolean MATCH_MULTI_LEVEL  = TOPIC.startsWith(SUBSCRIBED_TOPIC.endsWith("#") ? SUBSCRIBED_TOPIC.substring(0, SUBSCRIBED_TOPIC.indexOf("#")) : SUBSCRIBED_TOPIC);

            // Update those clients that subscribed to the topic of the current message
            if (MATCH_TOPIC || MATCH_SINGLE_LEVEL || MATCH_MULTI_LEVEL) {
                try {
                    subscriber.send(JSON.toString(), MediaType.APPLICATION_JSON);
                } catch (IOException e) {
                    subscriber.complete();
                    SUBSCRIBERS.remove(subscriber);
                }
            }
        });
    }

    public void addEmitter(final SseEmitter emitter) {
        emitters.add(emitter);
    }

    public void removeEmitter(final SseEmitter emitter) {
        emitters.remove(emitter);
    }

    //@Async
    //@Scheduled(fixedRate = 2000)
    @EventListener(condition = "#event.getTopic() == 'party.createdOrUpdated'")
    public void doNotify(NotificationEvent event) throws IOException {
        String partyData = partyManager.getPartyData().toString();
        System.out.println("Party count" + partyManager.findAll().size());
        System.out.println("Current thread name " + Thread.currentThread().getName());
        List<SseEmitter> deadEmitters = new ArrayList<>();
        emitters.forEach(emitter -> {
            try {
                System.out.println("Sending SSE Notification to client");
                emitter.send(SseEmitter.event()
                        .id(UUID.randomUUID().toString())
                        .name("party-addedorupdated")
                        .data(partyData)
                );
            } catch (Exception e) {
                System.out.println("Exception occurred while send to SseEmitter " + e.getMessage());
                deadEmitters.add(emitter);
            }
        });
        emitters.removeAll(deadEmitters);
    }

    private Pattern addPattern(final String TOPIC) {
        if (PATTERNS.containsKey(TOPIC)) return PATTERNS.get(TOPIC);
        final String[]      SUB_TOPICS      = TOPIC.split("/");
        final StringBuilder PATTERN_BUILDER = new StringBuilder();
        IntStream.range(0, SUB_TOPICS.length).forEach(i -> PATTERN_BUILDER.append(i == 0 ? "" : "\\/")
                .append("(")
                .append(SUB_TOPICS[i].equals("+") ? "(((\\+)|(\\w)*))" : SUB_TOPICS[i])
                .append(")"));
        Pattern PATTERN = Pattern.compile(PATTERN_BUILDER.toString());
        PATTERNS.put(TOPIC, PATTERN);
        return PATTERN;
    }
}
