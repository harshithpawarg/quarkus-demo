package com.demo.common.service;

import com.demo.common.model.Party;
import com.demo.common.repository.PartyRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class PartyManager {

    @Autowired
    private PartyRepository partyRepository;

    @Autowired
    private MqttManager mqttManager;

    //@Autowired
    //private NotificationEventPublisher notificationEventPublisher;

    @Autowired
    public void setOnMessageReceived() {
        System.out.println("fffffffffffffffffffffffffffffffffffff");
        mqttManager.setOnMessageReceived(event -> {
            TenantContext.setCurrentTenant("demo");
            System.out.println("sssssssssssssssssssss" + event.MESSAGE.toString());
            JSONObject jsonObject = new JSONObject(event.MESSAGE.toString());
            String action = jsonObject.getString("action");
            if (action != null) {
                if (action.equals("fetchAllParties")) {
                    sendPartyDetails();
                } else if (action.equals("partyUpdate")) {
                    savePartyDetails(jsonObject);
                }
            }
        });
    }

    private void savePartyDetails(JSONObject jsonObject) {
        JSONObject partyRecord = jsonObject.getJSONObject("partyDetails");
        Party party = getParty(Long.parseLong(partyRecord.get("id").toString()));
        party.setEnable(Boolean.parseBoolean(partyRecord.get("enabled").toString()));
        save(party);
//        notificationEventPublisher.publish("party.createdOrUpdated", party);
        sendPartyDetails();
    }

    public void sendPartyDetails() {
        JSONObject data = new JSONObject();
        data.put("action", "party.data");
        data.put("partyDetails", getPartyData());
        mqttManager.publish("mobile.subscriber", 2, data.toString(), true);
    }


    public List<Party> findAll() {
        return partyRepository.findAll();
    }

    public List<Map<String, Object>> getAllParties() {
        List<Party> parties = partyRepository.findAll();
        List<Map<String, Object>> result = new ArrayList<>();
        for (Party party : parties) {
            Map<String, Object> record = new HashMap<>();
            record.put("id", party.getId());
            record.put("name", party.getName());
            record.put("email", party.getEmail());
            record.put("phoneNumber", party.getPhoneNumber());
            record.put("isBroker", party.getIsBroker());
            record.put("isCollector", party.getIsCollector());
            record.put("isFunder", party.getIsFunder());
            record.put("isRepresentative", party.getIsRepresentative());
            record.put("isHouseBroker", party.getIsHouseBroker());
            record.put("isExternalFunder", party.getIsExternalFunder());
            record.put("isInvestor", party.getIsInvestor());
            record.put("isHouseCompany", party.getIsHouseCompany());
            record.put("isMerchant", party.getIsMerchant());
            record.put("accountNumber", party.getAccountNumber());
            record.put("abaNumber", party.getAbaNumber());
            record.put("funderPrefix", party.getFunderPrefix());
            record.put("contactPerson", party.getContactPerson());
            record.put("enabled", party.getEnable());
            record.put("bankName", party.getBankName() != null ? party.getBankName() : "");

            record.put("street", party.getPhysicalAddress() != null ? party.getPhysicalAddress().getAddress() : "");
            record.put("city", party.getPhysicalAddress() != null ? party.getPhysicalAddress().getCity() : "");
            record.put("zipcode", party.getPhysicalAddress() != null ? party.getPhysicalAddress().getZipCode() : "");
            result.add(record);
        }
        return result;
    }

    public Map<String, Object> findByPartyId(Long partId) {
//        return partyRepository.findByPartyId(partId).get(0);
        Party party = partyRepository.findById(partId).get();
        Map<String, Object> record = new HashMap<>();
        record.put("id", party.getId());
        record.put("name", party.getName());
        record.put("email", party.getEmail());
        record.put("phoneNumber", party.getPhoneNumber());
        record.put("isBroker", party.getIsBroker());
        record.put("isCollector", party.getIsCollector());
        record.put("isFunder", party.getIsFunder());
        record.put("isRepresentative", party.getIsRepresentative());
        record.put("isHouseBroker", party.getIsHouseBroker());
        record.put("isExternalFunder", party.getIsExternalFunder());
        record.put("isInvestor", party.getIsInvestor());
        record.put("isHouseCompany", party.getIsHouseCompany());
        record.put("isMerchant", party.getIsMerchant());
        record.put("accountNumber", party.getAccountNumber());
        record.put("abaNumber", party.getAbaNumber());
        record.put("funderPrefix", party.getFunderPrefix());
        record.put("contactPerson", party.getContactPerson());
        record.put("enabled", party.getEnable());
        record.put("bankName", party.getBankName() != null ? party.getBankName() : "");

        record.put("street", party.getPhysicalAddress() != null ? party.getPhysicalAddress().getAddress() : "");
        record.put("city", party.getPhysicalAddress() != null ? party.getPhysicalAddress().getCity() : "");
        record.put("zipcode", party.getPhysicalAddress() != null ? party.getPhysicalAddress().getZipCode() : "");
        return record;
    }

    public Party getParty(Long id) {
        return partyRepository.findById(id).get();
    }

    public void save(Party party) {
        partyRepository.save(party);
    }

    public JSONArray getPartyData() {
        List<Map<String, Object>> resultSet = getAllParties();
        JSONArray jsonArray = new JSONArray(resultSet);
        return jsonArray;
    }
}
