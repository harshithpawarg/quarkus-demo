package com.demo.common.service;

import com.demo.common.model.UserProfile;
import com.demo.common.repository.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class UserProfileManager {

    @Autowired
    private UserProfileRepository userProfileRepository;

    public UserProfile  getUserProfileByName(String name) {
        UserProfile userProfile = null;
        List<UserProfile>  userProfileList = userProfileRepository.findByEntityName(name);
        if(userProfileList.size() > 0) {
            userProfile = userProfileList.get(0);
        }
        return userProfile;
    }

    public Map<String, Object> createOrUpdateUserProfile(String name, Map<String, Object> info){
        UserProfile userProfile  = getUserProfileByName(name);
        if(userProfile == null){
            userProfile = new UserProfile();
        }
        userProfile.setEntityName(name);
        userProfile.setInfo(info);
        userProfileRepository.save(userProfile);
        return userProfile.getInfo();
    }
}
