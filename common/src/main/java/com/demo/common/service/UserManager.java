package com.demo.common.service;

import com.demo.common.model.Report;
import com.demo.common.model.Role;
import com.demo.common.model.Tenant;
import com.demo.common.model.User;
import com.demo.common.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.math.BigDecimal;
import java.util.*;

@Component
public class UserManager {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JasperReportGeneratorService jasperReportGeneratorService;

    public User getUser(Long userId) {
        User user = null;
        Optional<User> optionalUser = userRepository.findById(userId);
        if (optionalUser.isPresent()) {
            user = optionalUser.get();
        }
        return user;
    }

    public Map<String, Object> getUserInfo(String email) {

        Map<String, Object> response = new HashMap<>();
        User user = userRepository.findByEmailIgnoreCase(email);
        if (user != null) {

            Tenant tenant = user.getTenant();

            Map<String, Object> tenantMap = new HashMap<>();
            tenantMap.put("id", tenant.getId());
            tenantMap.put("name", tenant.getName());

            List<String> roles = new ArrayList<>();
            List<Role> roleList = user.getRoles();
            for(Role role : roleList) {
                roles.add(role.getName());
            }

            Map<String, Object> userSummary = new HashMap<>();
            userSummary.put("id", user.getId());
            userSummary.put("email", user.getEmail());
            userSummary.put("password", user.getPassword());
            userSummary.put("userName", user.getUserName());
            userSummary.put("firstName", user.getFirstName());
            userSummary.put("lastName", user.getLastName());
            userSummary.put("fullName", user.getFullName());
            userSummary.put("tenant", tenantMap);
            userSummary.put("roles", roles);

            response.put("userSummary", userSummary);
        }

        return response;
    }


    public String generateLetter(Report report, Map<String, Object> attr) throws Exception {
        String uuidToken = UUID.randomUUID().toString();

        String fileName = "report" + "_" + uuidToken + ".pdf";
        String outputFileName = "../resources/jasperReport/" + fileName;

        jasperReportGeneratorService.generatePdfReport(outputFileName, report.getFilePath(), attr);
        return outputFileName;
    }
}
