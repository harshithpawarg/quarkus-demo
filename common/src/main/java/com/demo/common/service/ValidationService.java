package com.demo.common.service;

import org.springframework.stereotype.Service;

@Service
public class ValidationService {
    public Boolean isPresent(Object value) {
        return (value != null && !value.toString().isEmpty());
    }
}
