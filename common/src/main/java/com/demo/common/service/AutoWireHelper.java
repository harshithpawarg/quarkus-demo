package com.demo.common.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class AutoWireHelper implements ApplicationContextAware {
    private static final AutoWireHelper INSTANCE = new AutoWireHelper();
    private static ApplicationContext applicationContext;

    private AutoWireHelper() {

    }


    public static void autowire(Object classToAutowire, Object... beansToAutowireInClass) {
        for (Object bean : beansToAutowireInClass) {
            if (bean == null) {
                applicationContext.getAutowireCapableBeanFactory().autowireBean(classToAutowire);
                return;
            }
        }
    }

    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) {
        AutoWireHelper.applicationContext = applicationContext;
    }

    public static AutoWireHelper getInstance() {
        return INSTANCE;
    }

}
