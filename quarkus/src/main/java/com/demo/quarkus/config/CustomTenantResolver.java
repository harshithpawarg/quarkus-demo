package com.demo.quarkus.config;

import com.demo.common.service.TenantContext;
import io.quarkus.arc.Unremovable;
import io.quarkus.hibernate.orm.runtime.tenant.TenantResolver;
import io.quarkus.oidc.OidcTenantConfig;
import io.vertx.ext.web.RoutingContext;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

@ApplicationScoped
@Unremovable
public class CustomTenantResolver implements TenantResolver {

    private static String DEFAULT_TENANT_ID = "demo";

    @Inject
    RoutingContext context;

    @Override
    public String getDefaultTenantId() {
        return "public";
    }

    @Override
    public String resolveTenantId() {
        String tenant = context.request().getHeader("tenant");
        String tenantId = TenantContext.getCurrentTenant() != null ? TenantContext.getCurrentTenant() : tenant;
        if (tenantId != null) {
            return tenantId;
        }
        return DEFAULT_TENANT_ID;
    }
}
